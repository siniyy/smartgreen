'use strict';

const express = require('express');
const http = require('http');
const path = require('path');
const app = express();

app.use(express.static(__dirname + '/dist'));

app.get('/site-list', function (req, res) {
  http
    .get('http://www.smartgreen.co.il/fed/sitesdata.json', (response) => {
      response.setEncoding('utf8');

      let rawData = '';
      response.on('data', chunk => rawData += chunk);
      response.on('end', () => {
        try {
          const data = JSON.parse(rawData);
          res.json(data);
        } catch(e) {
          console.log('Error parsing sites data');
          res.sendStatus(500);
        }
      });
  })
  .on('error', (err) => {
    console.log(err);
    res.sendStatus(500);
  });
});

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname + '/dist/index.html'));
});

app.listen(process.env.PORT || 8080);