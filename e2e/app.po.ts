import { browser, by, element, $ } from 'protractor';

export class SmartGreenPage {
  navigateTo(url = '/') {
    return browser.get(url);
  }

  getUrl() {
    return browser.getCurrentUrl();
  }

  getSiteElements() {
    return element.all(by.tagName('mat-grid-tile'));
  }

  openSiteOverview() {
    return $('mat-grid-tile:nth-child(2)').click();
  }

  openEquipment() {
    return $('mat-row:first-of-type').click();
  }

  getEquipmentElements() {
    return element.all(by.tagName('mat-row'));
  }

  getChartElement() {
    return element(by.css('.csschartjs-render-monitor'));
  }

  clickBreadcrumbsPrev() {
    return element.all(by.css('breadcrumb a')).last().click();
  }
}
