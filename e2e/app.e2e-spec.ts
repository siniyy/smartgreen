import {SmartGreenPage} from './app.po';
import {HttpBackend} from 'httpbackend';

// TODO should work with moch data

describe('smart-green App', () => {
  let page: SmartGreenPage;

  function getSitesMatchRegExp(extra = '') {
    return new RegExp('https?:\/\/.+\/sites' + extra);
  }

  beforeEach(() => {
    page = new SmartGreenPage();
    // return page.waitAppInit();
    // page.sleep();
  });

  it('should redirect to sites page', () => {
    page.navigateTo();
    expect(page.getUrl()).toMatch(getSitesMatchRegExp());
  });

  it('should display sites', () => {
    expect(page.getSiteElements().count()).toBeTruthy();
  });

  it('should navigate to equip details page', () => {
    page.openSiteOverview();
    expect(page.getUrl()).toMatch(getSitesMatchRegExp('/[\\w-]+'));
  });

  it('should display equipments', () => {
    expect(page.getEquipmentElements().count()).toBeTruthy();
  });

  // TODO
  it('should filter equipments', () => {});
  it('should sort equipments', () => {});
  it('should paginate equipments', () => {});

  it('should navigate to equipment page', () => {
    page.openEquipment();
    expect(page.getUrl()).toMatch(getSitesMatchRegExp('\/[\\w-]+\/equip\/\\w+'));
  });

  it('should show chart', () => {
    expect(page.getChartElement()).toBeTruthy();
  });

  it('should navigate back via breadcrumbs', () => {
    page.clickBreadcrumbsPrev();
    expect(page.getUrl()).toMatch(getSitesMatchRegExp('\/[\\w-]+'));

    page.clickBreadcrumbsPrev();
    expect(page.getUrl()).toMatch(getSitesMatchRegExp());
  });
});
