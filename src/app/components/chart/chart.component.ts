import {Component, ElementRef, Input, OnInit} from '@angular/core';
import {Chart} from 'chart.js';

@Component({
  selector: 'chart',
  template: '<div class="canvas-holder"><canvas></canvas></div>',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit{
  container: HTMLBaseElement;

  @Input() data: any = {};

  constructor(private elemRef: ElementRef) {
  }

  ngOnInit() {
    this.container = this.elemRef.nativeElement.querySelector('canvas');
    this.initChart();
  }

  initChart() {
    return new Chart(this.container, {
      type: 'line',
      data: this.data,
      options: {
        scales: {
          xAxes: [{
            display: true
          }],
          yAxes: [{
            display: true
          }]
        }
      }
    });
  }
}
