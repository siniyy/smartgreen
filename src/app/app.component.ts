import {Component} from '@angular/core';
import {BreadcrumbService} from 'ng5-breadcrumb';
import {NavigationStart, Router} from '@angular/router';
import {MobileService} from './services/mobile/mobile.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  opened = false;
  isMobile: boolean;

  menu = [{
    icon: 'business',
    title: 'Sites',
    link: '/sites'
  }, {
    icon: 'settings',
    title: 'Settings',
    link: '/settings'
  }];

  constructor(
    private breadcrumbService: BreadcrumbService,
    private router: Router,
    private mobileService: MobileService
  ) {
    this.configureBreadcrumbs();

    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        window.scrollTo(0, 0);
      }
    });

    this.isMobile = this.mobileService.isMobile();
  }

  configureBreadcrumbs() {
    this.breadcrumbService.addFriendlyNameForRoute('/settings', 'Settings');
    this.breadcrumbService.addFriendlyNameForRoute('/sites', 'Sites');
    this.breadcrumbService.addCallbackForRouteRegex('/sites/[\\w-]+$', (id) => {
      const [, site, block] = id.match(/(\w+)-(\d+)/);
      return `${site} (bl.${block})`;
    });
    this.breadcrumbService.hideRouteRegex('/sites/[\\w-]+/equip$');
  }

  navigate(link) {
    this.opened = false;
    this.router.navigate([link]);
  }
}
