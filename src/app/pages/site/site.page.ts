import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'sites-page',
  template: '<router-outlet></router-outlet>'
})
export class SitePage implements OnInit {
  constructor() {
  }

  ngOnInit() {
    const preloader = document.getElementById('preloader');

    if (preloader) {
      preloader.remove();
    }
  }
}
