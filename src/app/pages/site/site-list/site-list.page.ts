import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ISite} from '../shared/site.model';
import {BreakpointObserver} from '@angular/cdk/layout';

@Component({
  selector: 'app-site-list',
  templateUrl: './site-list.page.html',
  styleUrls: ['./site-list.page.scss']
})
export class SiteListPage implements OnInit {
  cols = 4;
  colsPrecision = '2:1';

  list: ISite[] = [];

  constructor(private route: ActivatedRoute, private breakpointObserver: BreakpointObserver) {
    breakpointObserver
      .observe([
        '(orientation: portrait)',
        '(orientation: landscape)'
      ])
      .subscribe((result) => {
        const isSmallScreen = breakpointObserver.isMatched('(max-width: 599px)');
        const isPortrait = breakpointObserver.isMatched('(orientation: portrait)');

        if (result.matches) {
          this.colsPrecision = '2:1';
          this.cols = 4;

          if (isSmallScreen) {
            this.colsPrecision = '3:2';

            if (isPortrait) {
              this.cols = 2;
            }
          }
        }
      });
  }

  ngOnInit() {
    this.list = this.route.snapshot.parent.data.list;
  }
}
