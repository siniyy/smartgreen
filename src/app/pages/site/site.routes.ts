import {Routes} from '@angular/router';
import {SitePage, SiteListPage, EquipListPage, EquipDetailsPage} from './';
import {SiteResolverService} from './shared/site.resolver.service';

export const siteRoutes: Routes = [{
  path: '',
  component: SitePage,
  resolve: {
    list: SiteResolverService
  },
  children: [{
    path: '',
    component: SiteListPage
  }, {
    path: ':id',
    children: [{
      path: '',
      component: EquipListPage
    }, {
      path: 'equip/:equip',
      component: EquipDetailsPage
    }]
  }]
}];
