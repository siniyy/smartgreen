import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {SitePage, SiteListPage, EquipListPage, EquipDetailsPage, SiteService} from './';
import {siteRoutes} from './site.routes';
import {SiteResolverService} from './shared/site.resolver.service';

import {MatGridListModule} from '@angular/material/grid-list';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';

import {ChartComponent} from '../../components/chart/chart.component'; // TODO move to components module

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(siteRoutes),
    MatGridListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule
  ],
  declarations: [
    SitePage,
    SiteListPage,
    EquipListPage,
    EquipDetailsPage,
    ChartComponent
  ],
  providers: [
    SiteService,
    SiteResolverService
  ]
})
export class SiteModule {
}
