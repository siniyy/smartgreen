import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

import {SiteService} from '../shared/site.service';
import {ISite} from '../shared/site.model';
import {IEquip} from '../shared/equip.model';


@Component({
  selector: 'app-site-overview',
  templateUrl: './equip-list.page.html',
  styleUrls: ['./equip-list.page.scss']
})
export class EquipListPage implements OnInit {
  site: ISite;
  tableColumns = ['equip_name', 'time', 'S3avg', 'S4avg', 'TempAvg'];
  dataSource: MatTableDataSource<IEquip>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private siteService: SiteService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.site = this.siteService.getById(this.route.snapshot.params.id);

    if (!this.site) {
      this.router.navigate(['404']);
    }

    this.dataSource = new MatTableDataSource(this.site.equips);
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim().toLowerCase();
    this.dataSource.filter = filterValue;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

}
