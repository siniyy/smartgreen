import {IEquip} from './equip.model';

export interface ISite {
  id?: string;
  site: string;
  block?: string;
  equips?: IEquip[];
};