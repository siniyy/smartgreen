import {TestBed, async} from '@angular/core/testing';

import { SiteService } from './site.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {ApiService, CacheService} from '@app/services';

describe('SiteService', () => {
  let service: SiteService;
  let mockbackend: HttpTestingController;

  const mockResponse = [{
    site: 'Item 1',
    equips: [{
      equip_name: 'test',
      time: '2018-01-01',
      S3avg: 1,
      S4avg: 1,
      TempAvg: 1
    }]
  }];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [SiteService, ApiService, CacheService]
    });

    service = TestBed.get(SiteService);
    mockbackend = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getSites()', () => {
    it('should return an Observable<Array<Site>>', async(() => {
      service
        .getSites({})
        .subscribe((sites) => {
          expect(sites[0].site).toBe('Item 1');
        });

      mockbackend
        .expectOne('/site-list')
        .flush(mockResponse, {status: 200, statusText: 'ok'});
    }));
  });
});