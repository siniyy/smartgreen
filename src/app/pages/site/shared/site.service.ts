import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {ISite} from './site.model';
import {catchError, tap, map} from 'rxjs/operators';
import {Observable} from 'rxjs/Observable';
import {ApiService, CacheType} from '@app/services';

const SITE_PASSED_PLURAL = {
  1: 'First',
  2: 'Second'
};

@Injectable()
export class SiteService {
  public list: ISite[] = [];

  constructor(private apiService: ApiService, private router: Router) {
  }

  getSites(options:any = {cache: CacheType.LocalStorage}) {
    return this.apiService
      .get('/site-list', options)
      .pipe(
        map((sites: ISite[]) => {
          const blocks = this.getBlocks(sites);

          this.list = sites
            .map((site) => {
              blocks[site.site].passed++;

              if (blocks[site.site].qty === 1) {
                site.block = 'Only'
              } else {
                site.block = SITE_PASSED_PLURAL[blocks[site.site].passed];
              }

              site.id = this.generateId(site, blocks);

              return site;
            })
            .sort((a, b) => a.site.localeCompare(b.site));

          return this.list;
        }),
        catchError(() => {
          this.router.navigate(['error']);
          return Observable.create([]);
        })
      );
  }

  getById(id) {
    return this.list.find(i => i.id === id);
  }

  getEquipment(params) {
    return this
      .getById(params.id)
      .equips
      .find(e => e.equip_name === params.equip);
  }

  private generateId(site, blocks) {
    return `${site.site}-${blocks[site.site].passed}`;
  }

  private getBlocks(sites) {
    return sites.reduce((res, i) => {
      if (!res[i.site]) {
        res[i.site] = {
          qty: 0,
          passed: 0
        };
      }
      res[i.site].qty++;

      return res;
    }, {});
  }
}
