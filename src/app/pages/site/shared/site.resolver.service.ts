import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {SiteService} from './site.service';
import {catchError} from 'rxjs/operators';

@Injectable()
export class SiteResolverService implements Resolve<any> {

  constructor(private siteService: SiteService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.siteService.getSites();
  }
}
