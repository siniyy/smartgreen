import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SiteService} from '../shared/site.service';
import {IEquip} from '../shared/equip.model';

@Component({
  selector: 'app-site-details',
  templateUrl: './equip-details.page.html',
  styleUrls: ['./equip-details.page.scss']
})
export class EquipDetailsPage implements OnInit {
  data: any;

  constructor(
    private siteService: SiteService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit() {
    const equipment = this.siteService.getEquipment(this.route.snapshot.params);

    if (!equipment) {
      this.router.navigate(['404']);
    }

    this.data = {
      labels: equipment.timeseries.map(i => i.hour),
      datasets: [
        {
          label: equipment.equip_name,
          data: equipment.timeseries.map(i => i.temperature),
          borderColor: '#673ab7',
          fill: false
        }
      ]
    }
  }
}
