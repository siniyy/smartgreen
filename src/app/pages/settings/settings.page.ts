import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss']
})
export class SettingsPage implements OnInit {
  isRtl = false;

  constructor() {
  }

  ngOnInit() {
    const preloader = document.getElementById('preloader');

    if (preloader) {
      preloader.remove();
    }

    this.isRtl = document.body.parentElement.getAttribute('dir') === 'rtl';
  }

  changeDir(event) {
    // only for test (not to interrupt toggle animation)
    setTimeout(() => {
      document.body.parentElement.setAttribute('dir', event.checked ? 'rtl' : 'ltr');
    }, 500);
  }
}