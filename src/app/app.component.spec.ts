import { TestBed, async } from '@angular/core/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';
import { AppComponent } from './app.component';
import {BreadcrumbService, Ng5BreadcrumbModule} from 'ng5-breadcrumb';
import {MatToolbarModule} from '@angular/material';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatListModule} from '@angular/material/list';
import {MobileService} from './services/mobile/mobile.service';
import {LayoutModule} from '@angular/cdk/layout';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        imports: [
          NoopAnimationsModule,
          RouterTestingModule.withRoutes([]),
          Ng5BreadcrumbModule.forRoot(),
          MatToolbarModule,
          MatButtonModule,
          MatButtonToggleModule,
          MatIconModule,
          MatSidenavModule,
          MatListModule,
          MatCardModule,
          MatSlideToggleModule,
          LayoutModule
        ],
        declarations: [
          AppComponent
        ],
        providers: [
          BreadcrumbService,
          MobileService
        ]
      })
      .compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should render breadcrumbs', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('breadcrumb')).toBeTruthy();
  }));
});
