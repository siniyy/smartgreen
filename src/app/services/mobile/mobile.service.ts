import {Injectable} from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';

@Injectable()
export class MobileService {
  constructor(private media: MediaMatcher) {
  }

  isMobile() {
    return this.media.matchMedia('(max-width: 600px)').matches;
  }
}
