import {TestBed, async} from '@angular/core/testing';

import { ApiService } from './api.service';
import {CacheService} from '@app/services';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('ApiService', () => {
  let service: ApiService;
  let mockbackend: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ApiService, CacheService]
    });

    service = TestBed.get(ApiService);
    mockbackend = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should have method GET', async(() => {
    const mockResponse = [{
      title: 'Item 1',
      id: 1
    }, {
      title: 'Item 2',
      id: 2
    }];

    service
      .get('/sites')
      .subscribe((next) => {
        expect(next).toEqual(mockResponse);
      });

    mockbackend
      .expectOne('/sites')
      .flush(mockResponse, {status: 200, statusText: 'ok'});
  }));
});
