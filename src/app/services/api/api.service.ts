import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

import {catchError, tap} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';

import {CacheService} from '../cache/cache.service';


@Injectable()
export class ApiService {
  constructor(private http: HttpClient, private cacheService: CacheService) {
  }

  public get(url: string, options: any = {}): Observable<any> {
    if (options.cache) {
      const data = this.cacheService.get(options.cache, url);

      if (data) {
        return of(data);
      }
    }

    return this.http
      .get(url)
      .pipe(
        tap((res) => {
          if (options.cache) {
            this.cacheService.set(options.cache, url, res);
          }
        }),
        catchError(this.handleError(null))
      );
  }

  private handleError<T>(_default) {
    return (error: any): Observable<T> => {
      console.error(error.message);

      return of(_default);
    }
  }
}
