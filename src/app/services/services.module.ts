import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';

import {ApiService, CacheService, MobileService} from './';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    ApiService,
    CacheService,
    MobileService
  ]
})
export class ServicesModule {
}
