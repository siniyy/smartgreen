import {Injectable} from '@angular/core';

export const enum CacheType {
  Application = 1,
  LocalStorage = 2
}

const LOCAL_STORAGE_LIVE_TIME = 24;

@Injectable()
export class CacheService {
  cacheObject: any;

  constructor() {
    this.cacheObject = {};
    this.initLocalStorage();
  }

  public get(cacheType, keyName) {
    let data = null;

    switch (cacheType) {
      case CacheType.Application:
        if (this.cacheObject[keyName]) {
          data = this.cacheObject[keyName];
        }
        break;

      case CacheType.LocalStorage:
        data = JSON.parse(window.localStorage.getItem(keyName));
        break;
    }

    return data;
  }

  public set(cacheType, keyName, data) {
    switch (cacheType) {
      case CacheType.Application:
        this.cacheObject[keyName] = data;
        break;

      case CacheType.LocalStorage:
        console.log(data)
        window.localStorage.setItem(keyName, JSON.stringify(data));
        break;
    }
  }

  public remove(cacheType, keyName) {
    switch (cacheType) {
      case CacheType.Application:
        if (this.cacheObject[keyName]) {
          delete this.cacheObject[keyName];
        }
        break;

      case CacheType.LocalStorage:
        window.localStorage.removeItem(keyName);
        break;
    }
  }

  public clearCache() {
    this.cacheObject = {};
    localStorage.clear();
  }

  private initLocalStorage() {
    const now = new Date().getTime();
    const setupTime = localStorage.getItem('setupTime');
    if (!setupTime || ((now - parseInt(setupTime, 10)) > (LOCAL_STORAGE_LIVE_TIME * 60 * 60 * 1000))) {
      localStorage.clear();
      localStorage.setItem('setupTime', now.toString());
    }
  }
}
