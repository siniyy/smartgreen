import {CacheService, CacheType} from './cache.service';

const testKey = 'test_key';
const testValue = 'test value';

describe('Cache Service', () => {
  let cacheService: CacheService;

  beforeEach(() => {
    cacheService = new CacheService();
  });

  it('should init storages', () => {
    expect(cacheService).toEqual(jasmine.objectContaining({cacheObject: {}}));
    expect(Object.keys(window.localStorage)).toEqual(['setupTime']);
  });


  it('should set / get Local storage value', () => {
    cacheService.set(CacheType.LocalStorage, testKey, testValue);
    expect(cacheService.get(CacheType.LocalStorage, testKey)).toEqual(testValue);
  });

  it('should set / get Application storage value', () => {
    cacheService.set(CacheType.Application, testKey, testValue);
    expect(cacheService.get(CacheType.Application, testKey)).toEqual(testValue);
  });


  it('should clear cache', () => {
    cacheService.set(CacheType.Application, testKey, testValue);
    cacheService.set(CacheType.LocalStorage, testKey, testValue);

    cacheService.clearCache();

    expect(cacheService.get(CacheType.Application, testKey)).toBe(null);
    expect(cacheService.get(CacheType.LocalStorage, testKey)).toBe(null);
  });
});
