import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

import {SettingsPage, NotFoundPage, ErrorPage} from './pages';

const routes: Routes = [{
  path: '',
  redirectTo: 'sites',
  pathMatch: 'full'
}, {
  path: 'sites',
  loadChildren: './pages/site/site.module#SiteModule'
}, {
  path: 'settings',
  component: SettingsPage
}, {
  path: '404',
  component: NotFoundPage
}, {
  path: 'error',
  component: ErrorPage
}, {
  path: '**',
  redirectTo: '404'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
